### BLAST Annotation

Each transcript has been aligned against the **_$blast_db$_** protein database using `blastx` program from the NCBI [BLAST] family.

For each transcript, the best BLAST hit is used to annotate its associated component/gene in further Differential Expression Results.

[The full BLAST results file is available here](blastx_Trinity_$blast_db$.tsv.zip)
